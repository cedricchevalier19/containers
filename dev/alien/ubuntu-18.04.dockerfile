# Ubuntu 18.04 for Alien

FROM ubuntu:18.04

LABEL maintener.email=<cedric.chevalier@cea.fr>

LABEL alien.repository.tag=dev-0.1
LABEL alien.ubuntu1804.version=dev-0.1

ARG DEBIAN_FRONTEND=noninteractive
ARG TZ=Europe/Paris

RUN apt-get update \
  && apt-get install -y ssh \
  build-essential \
  ccache \
  gcc \
  g++ \
  gdb \
  clang \
  rsync \
  tar \
  python \
  apt-transport-https \
  ca-certificates \
  gnupg \
  software-properties-common \
  wget \
  && apt-get clean

## Add kitware repo for up-to-date cmake
RUN wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | apt-key add -

RUN apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main' \
  && apt-get update && apt-get install -y cmake \
  && apt-get clean

## Install linear algebra libraries for alien.
RUN apt-get install -y \
  petsc-dev \
  libhypre-dev \
  libsuperlu-dist-dev \
  libboost-dev \
  libboost-program-options-dev \
  libgtest-dev \
  libglib2.0-dev \
  doxygen \
  python3-breathe sphinx-rtd-theme-common \
  && apt-get clean && rm -rf /var/lib/apt/lists/*

# gtest package contains only src, we need to compile it
RUN cd $(mktemp -d) && \
  cmake /usr/src/googletest && \
  cmake --build . --target install

RUN mkdir -p /compilation && chmod 1777 /compilation

ENV HOME /home/user

RUN useradd -m user --home $HOME

VOLUME /compilation

USER user
COPY --chown=user common/files/ccache.conf $HOME/.ccache/

CMD ["/bin/bash", "-l"]
